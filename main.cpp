#include <iostream>
#include <string>
#include <windows.h>

bool isPalRec(std::string word, int f, int l)
{
	if (f == l)
		return true;

	if (word[f] != word[l])
		return false;

	if (f < l + 1)
	{
		return isPalRec(word, f + 1, l - 1);
		return true;
	}
}

bool isPalindrome(std::string word)
{
	int n = word.length();

	if (n == 0)
		return true;

	return isPalRec(word, 0, n - 1);
}

int main()
{
	setlocale(LC_ALL, "Rus");
	SetConsoleCP(1251);

	std::string word;
	std::cin >> word;
	std::cout << "�������� �� ����� " << word << " �����������?" << std::endl;

	if (isPalindrome(word))
		std::cout << "��" << std::endl;
	else
		std::cout << "���" << std::endl;

	return 0;
}